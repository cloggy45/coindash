# Coin:Dash

[![CircleCI](https://circleci.com/gh/cloggy45/CoinDash.svg?style=svg)](https://circleci.com/gh/cloggy45/CoinDash)

[![codecov](https://codecov.io/gh/cloggy45/CoinDash/branch/master/graph/badge.svg)](https://codecov.io/gh/cloggy45/CoinDash)

##### To install
- Run `npm install`
- Add Firebase credentials to your `.env` file, for convenience, you can make sure they have the variable names already in the `firebase-config` file. 
- For a development build `npm start`
